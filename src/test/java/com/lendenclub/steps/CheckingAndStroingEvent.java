package com.lendenclub.steps;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

public class CheckingAndStroingEvent {

	WebDriver driver;
	List<WebElement> countrow;
	List<WebElement> countrow1;
	String event[]=new String[100],columName[]= {"Event Name","Event ID  ","Event date & Time "};
	String version[]= new String[20];
	String Row[] = { " ", " ", " " };
	CSVWriter writer;

	public CheckingAndStroingEvent(WebDriver driver) throws IOException, FileNotFoundException, CsvValidationException, InterruptedException 
	{
		this.driver=driver;	

		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/ul/li[1]/a")).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.linkText("All Settings")).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Testing Console")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		String[] values;
		CSVReader reader=new CSVReader(new FileReader("./ReadExcel/borrower_device_id.csv"));
		
		while ((values = reader.readNext()) != null) 
		{	
			for(int a=0; a<values.length; a++)
			{
				System.out.print(values[a] + " ");	

				driver.findElement(By.id("inputId")).clear();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.id("inputId")).sendKeys(values);

				try {
					driver.findElement(By.xpath("//*[ contains (text(), 'View device data' ) ]")).click();
				}catch(Exception e)  { }
				try {
					driver.findElement(By.xpath("//*[ contains (text(), 'Inspect device' ) ]")).click();
				} catch (Exception e) {
				}

				try {
					WebDriverWait wait = new WebDriverWait(driver, 4);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
				} catch (Exception ex) {
					ex.getMessage();
				}

				driver.manage().window().fullscreen();
				//for scroll to view all event id 
//				writer=new CSVWriter(new FileWriter("./Resources/BorrowerStatus "+values[a]+".csv"));
				writer = new CSVWriter(new FileWriter("./Resources/BorrowerStatus.csv", true));
				
				String error=null;
				try {
					error=(driver.findElement(By.xpath("//*[@id=\"angular-sidebar-portal\"]/div/div/div/p")).getText().toString());
					
				}catch(Exception e) {}
				System.out.println(error);
				
				if( error != null)
				{  	
					System.out.println(" am inside the errror");
					String failColumnName[]=new String[10];
					failColumnName[0]="Device-Id";
					failColumnName[1]="Status";
					failColumnName[2]="Reason";
					writer.writeNext(failColumnName);

					failColumnName[2]="Fail";
					failColumnName[1]=driver.findElement(By.className("react-modal__content__message")).getText();
					failColumnName[0]=values[a];
					writer.writeNext(failColumnName);
					driver.findElement(By.xpath("//*[@id=\"angular-sidebar-portal\"]/div[1]/div/div/div/button")).click();
						

				}
				else {
					
					countrow=(List<WebElement>) driver.findElements(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[10]/div/div"));
				    int rowsize=countrow.size();
					
					try {
						JavascriptExecutor js = (JavascriptExecutor) driver;		
						WebElement Element = driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[2]/button"));
						js.executeScript("arguments[0].scrollIntoView();", Element);
					} catch(Exception e) {}
					
					version[0]="Device-Id";
					version[1]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[2]/div/div[2]")).getText().toString();

					writer.writeNext(version);

					int m=0;
					for(int n=1;n<=2; n++)
					{
						event[m]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[4]/div/div["+n+"]")).getText();
						version[m]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[7]/div/div["+n+"]")).getText();
						m++;
					}

					writer.writeNext(event);
					writer.writeNext(version);

					writer.writeNext(columName);
					for(int i=2; i<=rowsize;i++)
					{
						for(int j=1;j<=2;j++)
						{
							event[j]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[10]/div/div["+i+"]/div["+j+"]")).getText();
						}
						event[0]=validateEventID(event[1]);
						
						writer.writeNext(event);
					}
				}
				
				writer.writeNext(Row);
				writer.flush();

			}
			
		}
	}	


	private String validateEventID(String test) {

		String eventName = null;

		if (test.contains("gnvwv5")) {
			eventName = "SIGN_UP_TOKEN";
		}
		else if (test.contains("mdrxnp")) {
			eventName = "SOCIAL_LOGIN_EVENT_TOKEN ";
		}
		else if (test.contains("dypky0")) {
			eventName = "PERSONAL_DETAILS_EVENT_TOKEN  ";
		}
		else if (test.contains("d6vjuz")) {
			eventName = "ELIGIBLE_EVENT_TOKEN";
		} 
		else if (test.contains("a4rb95")) {
			eventName = "LOAN_REQUIREMENT_EVENT_TOKEN ";
		} 
		else if (test.contains("r9gg6y")) {
			eventName = "REGISTRATION_FEES_CONFIRMATION_TOKEN ";
		}
		else if (test.contains("n4fsxu")) {
			eventName = "LIVE_KYC_EVENT_TOKEN";
		}
		else if (test.contains("tz4fwx")) {
			eventName = "RESIDENTIAL_DETAILS_EVENT_TOKEN";
		} 
		else if (test.contains("wr6olb")) {
			eventName = "CREDIT_DETAILS_EVENT_TOKEN  ";
		} 
		else if (test.contains("d6fkc8")) {
			eventName = "BANK_STATEMENT_EVENT_TOKEN  ";
		}
		else if (test.contains("ka63bc")) {
			eventName = "BANK_DETAILS_EVENT_TOKEN ";
		}
		else if (test.contains("izm72m")) {
			eventName = "AUTO_DEBIT_EVENT_TOKEN ";
		} 
		else if (test.contains("uw8uoe")) {
			eventName = "LOAN_AGREEMENT_EVENT_TOKEN ";
		} 
		else if (test.contains("ka63bc")) {
			eventName = "BANK_DETAILS_EVENT_TOKEN ";
		}
		else if(test.contains("b8ebd9")){
			eventName ="CANCELLED_LOAN_EVENT_TOKEN";
		}
		else if(test.contains("u65anr"))
		{
			eventName ="CLOSED_LOAN_EVENT_TOKEN";
		}
		else if(test.contains("sttovk"))
		{
			eventName ="DECLINED_LOAN_EVENT_TOKEN";
		}
		else if(test.contains("om5n31"))
		{
			eventName ="DISBURSED_LOAN_EVENT_TOKEN";
		}
		else if(test.contains("cs62tl"))
		{
			eventName ="LISTED_LOAN_EVENT_TOKEN";
		}
		else if(test.contains("1h1e4z"))
		{
			eventName ="OPEN_LOAN_EVENT_TOKEN";
		}
		else if(test.contains("pyekiv"))
		{
			eventName ="PROCESSING_LOAN_EVENT_TOKEN";
		}
		else {

			eventName="To be taken from Amir shaikh";
		}



		return eventName;

	}

}
