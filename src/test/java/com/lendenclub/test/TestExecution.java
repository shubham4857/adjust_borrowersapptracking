package com.lendenclub.test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.lendenclub.selenium.WebCapability;
import com.lendenclub.steps.CheckingAndStroingEvent;
import com.lendenclub.steps.Login;

public class TestExecution extends WebCapability{
	
	WebDriver driver;
	
	@BeforeTest
	public void openingFireFox()
	{	   
		driver = WebCapability();
	}
	
	@Test(priority = 1)
	public void  loginPage() throws Exception
	{
	  	new Login(driver);
	  	
	}
	
	@Test(priority = 2)
	public void  ChechingJobStatus() throws Exception
	{
	  	new CheckingAndStroingEvent(driver);
	  	
	}
	
	@AfterTest
	public void CloseBrowser()
	{
		
	 	driver.quit();
	}

}
